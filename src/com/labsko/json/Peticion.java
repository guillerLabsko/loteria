package com.labsko.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Peticion {
	
	
	private String link ="http://api.elpais.com/ws/LoteriaNavidadPremiados?n=";
	
	private String numero;
	private int premio;
	private int estado; // sorteo no ha comenzado == 0 
		//sorteo ha empezado, premios parcial == 1
		//sorteo ha terminado, premios provisional == 2
		//sorteo ha terminado, premios semioficial == 3
		//sorteo ha terminado, premios oficial == 4
	private int error; // error==1
	
	
	public Peticion(String numero){
		this.numero = numero;
		link = link + numero;
		inicializar();
		
		error = 0;
		estado = 4;
		premio = 0;
	}
	
	private String getRespuesta() throws IOException{
		URL url = new URL(link);
		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
		String entrada;
		String cadena="";
		while ((entrada = br.readLine()) != null){
			cadena = cadena + entrada;
		}
		return cadena;
	}
	
	public String getNumero() {
		return numero;
	}


	public int getPremio() {
		return premio;
	}


	public int getEstado() {
		return estado;
	}


	public int getError() {
		return error;
	}

	
	private void inicializar(){
		try {
			
			
			String respuesta = getRespuesta();
			respuesta = respuesta.replace("busqueda=", "");
				
			JSONObject oJson = new JSONObject(respuesta);
			
			this.error = oJson.getInt("error");
			this.estado = oJson.getInt("status");
			this.premio = oJson.getInt("premio");
			
			Log.i("url",link);
			Log.i("Respuesta", respuesta);
			Log.i("error", String.valueOf(oJson.getInt("error")));
			Log.i("status", oJson.getString("status"));
			Log.i("premio", oJson.getString("premio"));
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
