package com.labsko.loteria;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class PremiadoActividad extends Activity{

	//desplegar un cálculo para saber el dinero ganado; dinero jugado * premio/20
	
	private int premio;
	private int estado;
	
	private int dinero_jugado;
	
	private TextView tvPremio;
	private TextView tvDineroGanado;
	private EditText eTeuros;
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.premiado);
		
		tvPremio = (TextView)this.findViewById(R.id.tVpremio);
		tvDineroGanado = (TextView)this.findViewById(R.id.tVdineroGanado);
		eTeuros = (EditText)this.findViewById(R.id.eTeuros);
		
		Bundle datos = this.getIntent().getExtras();
		premio = datos.getInt(Utils.premio);
		estado = datos.getInt(Utils.estado);
			
		tvPremio.setText("El premio es de: "+premio+"€ por decimo jugado. ");
			
	}
	
	public void calculo(View v){
		int dinero_ganado = 0;

		dinero_jugado = Integer.parseInt(eTeuros.getText().toString());
	
		dinero_ganado = dinero_jugado * (premio/20);
		
		tvPremio.setText("Ha ganado en total: "+dinero_ganado+"€");
		
		//tvDineroGanado.setText("Ha ganado en total: "+ dinero_jugado);
	}
	

}
