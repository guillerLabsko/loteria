package com.labsko.loteria;

import com.labsko.json.Peticion;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private EditText numero;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Strict Mode, se deber�a utilizar hilos de ejecuci�n
		// http://developer.android.com/reference/android/os/StrictMode.html
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		// FIN lineas de código
		
		
		numero = (EditText)this.findViewById(R.id.editText1);
		
			
	}

	public void resultado(View v){
		Peticion peticion = new Peticion((String)numero.getText().toString());
		
		if (peticion.getError() == 1) {
			Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG)
					.show();
		} else {

			if (peticion.getEstado() == 0) {
				Toast.makeText(getApplicationContext(), "Concurso no iniciado",
						Toast.LENGTH_LONG).show();
			} else {
				if(peticion.getPremio() == 0){
					Intent intento = new Intent(this,NoPremiadoActividad.class);
					this.startActivity(intento);
				}
				else{
					Intent intento = new Intent(this,PremiadoActividad.class);
					intento.putExtra(Utils.premio, peticion.getPremio());
					intento.putExtra(Utils.estado, peticion.getPremio());
					this.startActivity(intento);
				}
			}
		}		
	}
	
	
	public void premiados(View v){//sin codificar
		//http://api.elpais.com/ws/LoteriaNavidadPremiados?n=resumen
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
