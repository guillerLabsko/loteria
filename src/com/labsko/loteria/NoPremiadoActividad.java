package com.labsko.loteria;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore.Video;
import android.widget.Toast;


public class NoPremiadoActividad extends Activity{
	
	private MediaPlayer mediaPlayer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nopremiado);
		  //Instancia al recurso
        mediaPlayer = MediaPlayer.create(NoPremiadoActividad.this,R.raw.sonido);
        //La musica empieza
        mediaPlayer.start();
        
        Toast.makeText(getApplicationContext(),"Boleto no premiado", Toast.LENGTH_LONG).show();
   
			
	}

	@Override
	protected void onPause() {
		super.onPause();
		mediaPlayer.pause();
	}

}
